/**
 * Created by Daren on 30/3/16.
 */
var express = require("express");
//for the first step in index.js, it will be installing all the different modules and that is done by code "require"//
// load express after you have done npm install --save express

/* we are using the express function() to create an application*/
var app = express();

/*our express application: all static files will come from that directory, ie use this directory "public" as doc root*/
app.use( express.static(__dirname + "/public") );

console.log("__dirname = " + __dirname);

/*in order to start our webserver on port 3000, we do the following: */
app.listen(3000, function () {
    console.info("Webserver has started on port 3000");
    console.info("Document root is at "+ __dirname + "/public");
    
});